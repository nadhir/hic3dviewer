from hicViewer.chrMesh import *
from hicViewer.spatialModel import *
from hicViewer import settings
from flask import current_app as app
from flask import session
from cooler import Cooler
from pathlib import Path
from werkzeug.utils import secure_filename

import numpy as np
import json
import os
import traceback
import pathlib




def getAvailableGenome():
    ## load the info.json in the data folder
    fileName = os.path.join(settings.DATA_DIR, "info.json")
    with open(fileName, 'r') as genomes_info:
        genomes_data = json.load(genomes_info)

    ## load the info.json in the session fold if it exists
    if ('uid' in session.keys()):
        fileName = os.path.join(settings.UPLOAD_FOLDER, str(session['uid']), 'model', "info.json")
        if (os.path.exists(fileName)):
            with open(fileName, 'r') as genomes_info:
                genomes_data2 = json.load(genomes_info)
            genomes_data.extend(genomes_data2)
    app.logger.info("returning available genomes")
    return genomes_data



def get_interaction(chromosomes, genome, isIntra = True):

    genomes = getAvailableGenome()

    ## Check if our genome is there
    species = [x for x in genomes if x['name'] == genome]
    if (len(species) > 0 ):
        mesh = None;
        ## if the it is a pre-built genome load it from the data folder otherwise load it from the user's folder
        if (species[0]['type'] == "prebuilt"):
            genomePath = os.path.join(settings.DATA_DIR, species[0]['name'])
            modelPath = os.path.join(genomePath, species[0]['file'])
            mesh = chrMesh(genomePath)
        else:
            genomePath = os.path.join(settings.UPLOAD_FOLDER, str(session['uid']), species[0]['name'])
            modelPath = os.path.join(settings.UPLOAD_FOLDER, str(session['uid']), 'model', species[0]['file'])
            mesh = chrMesh(genomePath)


        resolution = species[0]['resolution']

        if isIntra:
            chromosomes = int(chromosomes.decode('utf-8'))
            maxFreq =mesh.getIntra(genomePath, modelPath, chromosomes, resolution)
            jsonRes = {"interactions": mesh.interactions, "maxFreq" : maxFreq}
            jsonRes = json.dumps(jsonRes)
        else:
            if chromosomes is not None:
                chromosomes = [int(x.decode('utf-8')) for x in chromosomes]
            maxFreq = mesh.getInter( genomePath, modelPath, resolution, chromosomes)
            jsonRes = {"interactions": mesh.transInteractions, "maxFreq" : maxFreq}
            jsonRes = json.dumps(jsonRes)
    else:
        jsonRes = json.dumps({"error": "Error while processing interactions"})
    return jsonRes


## Check if the provided coordinate data structure is ok.
def checkCoords(data):

    if len(["xcoord", "ycoord", "chr1", "chr2"] & data.viewkeys()) != 4:
        return False

    if len(data["xcoord"])== 0 or len(data["ycoord"]) == 0:
        return False

    if len(data["xcoord"]) != len(data["ycoord"]):
        return False

    return True


def extractChrName( root ,filePath):

    import numpy as np

    original_lengths = np.genfromtxt(filePath, dtype= None)

    plengths  = os.path.join(root, "chr_lengths.txt")
    pchrNames = os.path.join(root, "chr_names.txt")


    flenghts = open(plengths,"w")
    fchrName = open(pchrNames, "w")
    for i in range(len((original_lengths))):
        flenghts.write("%s\n" % original_lengths[i][1])
        fchrName.write("%s\t%s\n" % (original_lengths[i][0],i))

    flenghts.close()
    fchrName.close()

    res = ["chr_lengths.txt", pchrNames]

    return res


def check_model_exists(filename, modelPath, sessionid, resolution):
    # check if the user already uploaded some models befoe
    modelInfoFile = os.path.join(modelPath, "info.json")
    modelInfo = []
    # if the some models exist then just load it
    if ( os.path.exists(modelInfoFile)):
        with open(modelInfoFile, 'r') as genomes_info:
            modelInfo = json.load(genomes_info)

        # check if the file existed before
        exists = [x for x in range(len(modelInfo)) if modelInfo[x]['file'] == filename]

        if (len(exists) > 0):
            modelInfo[exists[0]]['resolution'] = resolution
        else:
            modelInfo.append({'resolution': resolution,
                              'name': os.path.splitext(os.path.basename(filename))[0],
                              'file': filename,
                              'modelPath': os.path.join("tmp",sessionid,"model"),
                              'type': 'user'})

    # if this is the first model then just create the file
    else:        
        modelInfo = [{'resolution': resolution,
                      'name': Path(filename).stem, #os.path.splitext(os.path.basename(filename))[0],
                      'file': filename,
                      'modelPath': os.path.join("tmp",sessionid,"model"),
                      'type': 'user'
                     }]
    # update the info.json file
    with open(modelInfoFile, 'w') as f:
        json.dump(modelInfo, f)

# upload the Hic matrix in cooler format
def upload_hic_cooler(root : Path,
                      hicfile : Path, 
                      hicFileName : str, 
                      dirName : Path,
                      resolution, alpha, beta, seed, method, normalize):

    try:
        ## check if we have any file uploaded in the directory   
        dirName.mkdir(parents=True, exist_ok=True)        

        hicUploadPath = dirName / hicFileName 
        hicfile.save(hicUploadPath)

        ## check if the file follow the cooler format
        if '.mcool' in Path(hicFileName).suffix:
            hicUploadPath =  hicUploadPath / '::{}'.format(resolution)  # os.path.join(os.fspath(hicUploadPath), '::' + resolution)
        
            
        f = Cooler(hicUploadPath)
        app.logger.info(f.info)

        app.logger.info("initiating a spatialModel class")
        modelprep = spatialModel()
        app.logger.info("spatialModel preparing folder")
        # modelprep.prepareFolder(root, resolution, alpha,beta,seed, chrlenFileName, hicFileName, method, normalize)
        app.logger.info("finished folder preparation and prediction")
        content = {'success': 1}
    except Exception as e:
        app.logger.error("sessionID: {0} Function: UploadHic Error: {1}".format(str(session["uid"]), e))
        app.logger.error(traceback.format_exc())
        content = {'error': "Error while uploading file"}        

    return json.dumps(content)
   
   
## upload the Hic matrix in old format (.bed)
def upload_hic_old(root, hicfile, hicFileName, dirName, lengthsFile, resolution, alpha, beta, seed, method, normalize):
    
    try:
        
        if not os.path.exists(dirName):
            os.mkdir(dirName)

        hicUploadPath = os.path.join(dirName, hicFileName)
        hicfile.save(hicUploadPath)

        ## upload the chromosome lengths file
        chrlenFileName = secure_filename(lengthsFile.filename)
        lengthsUploadPath = os.path.join(dirName, chrlenFileName)

        lengthsFile.save(lengthsUploadPath)

        ## check how many columns are in the lengths file
        f = open(lengthsUploadPath)
        line = f.readline()
        line = line.strip()
        line = line.split()

        if(len(line)>2):
            content = {"error": "The lengths file should contain a maximum of 2 columns"}
            Exception(content)

        if(len(line) ==2):
            f.close()
            res = extractChrName(dirName, lengthsUploadPath)
            chrlenFileName = res[0]
        else:
            pname = os.path.join(dirName, "chr_names.txt")
            fname = open(pname, "w")
            lengths = np.loadtxt(lengthsUploadPath)
            for i in range(lengths.shape[0]):
                fname.write("chr%s\t%s\n" % (i+1, i+1))

            fname.close()


        app.logger.info("initiating a spatialModel class")
        modelprep = spatialModel()
        app.logger.info("spatialModel preparing folder")
        modelprep.prepareFolder(root, resolution, alpha,beta,seed, chrlenFileName, hicFileName, method, normalize)
        app.logger.info("finished folder preparation and prediction")
        content = {'success': 1}
    except Exception as e:
        app.logger.error("sessionID: {0} Function: UploadHic Error: {1}".format(str(session["uid"]), e))
        app.logger.error(traceback.format_exc())
        content = {'error': "Error while uploading file"}        

    return json.dumps(content)
